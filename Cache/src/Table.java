public class Table {
	
	private int Id;
	private int EnumId;
	private String Code;
	private String value;
	private EnumerationName EnumName;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getEnumId() {
		return EnumId;
	}
	public void setEnumId(int enumId) {
		EnumId = enumId;
	}
	public String getCode() {
		return Code;
	}
	public void setCode(String code) {
		Code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public EnumerationName getEnumName() {
		return EnumName;
	}
	public void setEnumNameRegion(EnumerationName enumName) {
		EnumName = EnumerationName.Region;
	}
	public void setEnumNameGender(EnumerationName enumName) {
		EnumName = EnumerationName.Gender;
	}
	

}